# mono-repository

###### This repo is an example of how lerna can be used to manage your own packages.

##Pros
- Versions are consistent and easily managed 
- You can work directly in the project and see your package changes in real time without updating package
- Peer dependencies are handled properly as it will always have the OA consumer

##Cons 
- Need to switch to yarn to use workspaces. 
- It'll be locked inside OA repo for dev (This might be okay?)
- It'll need special config for TypeScript but it is possible. Just more work

#How it Works

- Lerna adds a packages folder at the root level for you to create your custom packages
- `yarn start` each package to create a `dist` folder that exports the src as a compiled version of your react comp. 
- Lerna uses the workspaces provided to work with your package directly from the dist without importing the dist itself. (ie: if you change a file in your package `src` itll update the root project so you can see your change)
- Once you're happy with the changes you push your changes and run `lerna publish` Lerna will detect what has changed in what packages and prompt you bump your version as a patch,minor or major release. It will then handle pushing tags and updating the registry as well as the package itself with the new version.

- Located in `/scripts/updateDeps` is a script that will run automatically after the publish completes. It will read the updated versions of the packages and install them in the root project inside the workspace so you can continue working and playing with the most up to date code. 

##### Note: the `updateDeps` script uses `jq` to parse json files

##### run this command to install jq 

`chocolatey install jq`

##### (I cant test this script I dont think I have access on my machine to run sh files but it should work as intended.)

##### Also Note: The packages are using react directly from the root `package.json` this will fix the issue will trying to build or run packages with peer dependencies with no consumer


