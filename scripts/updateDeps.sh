LATEST_GRID=$(jq -r '.version' ./packages/grid/package.json)
LATEST_CONTROLS=$(jq -r '.version' ./packages/controls/package.json)


echo "
*******************
Updating grid to version $LATEST_GRID
Updating controls to version $LATEST_CONTROLS
*******************"

yarn add \
  @catman/grid@$LATEST_GRID -W \
  @catman/controls@$LATEST_CONTROLS -W \

git add package.json

echo "package.json is ready to be commited"
echo "git commit -m \"Publish Packages\""